<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ledcube">
<packages>
<package name="HDRV2W80P254_1X2">
<pad name="1" x="-1.27" y="0" drill="1.3" diameter="1.95"/>
<pad name="2" x="1.27" y="0" drill="1.3" diameter="1.95"/>
<wire x1="2.6" y1="1.35" x2="-2.6" y2="1.35" width="0.127" layer="21"/>
<wire x1="-2.6" y1="1.35" x2="-2.6" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-1.35" x2="2.6" y2="-1.35" width="0.127" layer="21"/>
<wire x1="2.6" y1="-1.35" x2="2.6" y2="1.35" width="0.127" layer="21"/>
<text x="-2.54" y="1.905" size="0.8128" layer="25">&gt;NAME</text>
<wire x1="-2.8" y1="1.55" x2="-2.8" y2="-1.55" width="0.127" layer="51"/>
<wire x1="-2.8" y1="-1.55" x2="2.8" y2="-1.55" width="0.127" layer="51"/>
<wire x1="2.8" y1="-1.55" x2="2.8" y2="1.55" width="0.127" layer="51"/>
<wire x1="2.8" y1="1.55" x2="-2.8" y2="1.55" width="0.127" layer="51"/>
</package>
<package name="HDRV2W80P254_1X4">
<pad name="1" x="-3.81" y="0" drill="1.3" diameter="1.95"/>
<pad name="2" x="-1.27" y="0" drill="1.3" diameter="1.95"/>
<wire x1="5.14" y1="1.35" x2="-5.14" y2="1.35" width="0.127" layer="21"/>
<wire x1="-5.14" y1="1.35" x2="-5.14" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-5.14" y1="-1.35" x2="5.14" y2="-1.35" width="0.127" layer="21"/>
<wire x1="5.14" y1="-1.35" x2="5.14" y2="1.35" width="0.127" layer="21"/>
<text x="-5.08" y="1.905" size="0.8128" layer="25">&gt;NAME</text>
<wire x1="-5.34" y1="1.55" x2="-5.34" y2="-1.55" width="0.127" layer="51"/>
<wire x1="-5.34" y1="-1.55" x2="5.34" y2="-1.55" width="0.127" layer="51"/>
<wire x1="5.34" y1="-1.55" x2="5.34" y2="1.55" width="0.127" layer="51"/>
<wire x1="5.34" y1="1.55" x2="-5.34" y2="1.55" width="0.127" layer="51"/>
<pad name="3" x="1.27" y="0" drill="1.3" diameter="1.95"/>
<pad name="4" x="3.81" y="0" drill="1.3" diameter="1.95"/>
</package>
<package name="CAPC2013X60N">
<description>0805</description>
<smd name="1" x="-0.95" y="0" dx="1.4" dy="0.95" layer="1" rot="R90"/>
<smd name="2" x="0.95" y="0" dx="1.4" dy="0.95" layer="1" rot="R90"/>
<wire x1="1.8" y1="0.95" x2="-1.8" y2="0.95" width="0.127" layer="21"/>
<wire x1="-1.8" y1="0.95" x2="-1.8" y2="-0.95" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.95" x2="1.8" y2="-0.95" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.95" x2="1.8" y2="0.95" width="0.127" layer="21"/>
<text x="-2.25" y="1.25" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="CAPRD350W60D800H1300P">
<pad name="1" x="-1.75" y="0" drill="0.8" diameter="1.3"/>
<pad name="2" x="1.75" y="0" drill="0.8" diameter="1.3"/>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<text x="-5.08" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<circle x="0" y="0" radius="4.25" width="0.127" layer="51"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.27" y2="1.905" width="0.127" layer="21"/>
</package>
<package name="SOIC127P600X175-8N">
<smd name="1" x="-2.7" y="1.905" dx="1.55" dy="0.6" layer="1"/>
<smd name="2" x="-2.7" y="0.635" dx="1.55" dy="0.6" layer="1"/>
<smd name="4" x="-2.7" y="-1.905" dx="1.55" dy="0.6" layer="1"/>
<smd name="3" x="-2.7" y="-0.635" dx="1.55" dy="0.6" layer="1"/>
<smd name="5" x="2.7" y="-1.905" dx="1.55" dy="0.6" layer="1"/>
<smd name="6" x="2.7" y="-0.635" dx="1.55" dy="0.6" layer="1"/>
<smd name="7" x="2.7" y="0.635" dx="1.55" dy="0.6" layer="1"/>
<smd name="8" x="2.7" y="1.905" dx="1.55" dy="0.6" layer="1"/>
<wire x1="-1.7" y1="2.405" x2="-1.7" y2="-2.495" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.495" x2="1.7" y2="-2.495" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.495" x2="1.7" y2="2.405" width="0.127" layer="21"/>
<wire x1="1.7" y1="2.405" x2="-1.7" y2="2.405" width="0.127" layer="21"/>
<circle x="-2.2225" y="3.015" radius="0.3175" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.75" x2="-3.75" y2="2.75" width="0.127" layer="51"/>
<wire x1="-3.75" y1="2.75" x2="-3.75" y2="-2.75" width="0.127" layer="51"/>
<wire x1="-3.75" y1="-2.75" x2="3.75" y2="-2.75" width="0.127" layer="51"/>
<wire x1="3.75" y1="2.75" x2="3.75" y2="-2.75" width="0.127" layer="51"/>
<text x="-0.635" y="3.175" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="PJ-036AH-SMT">
<hole x="-3.8" y="0" drill="2"/>
<smd name="3" x="0" y="6.45" dx="6.2" dy="3.9" layer="1"/>
<smd name="2" x="0" y="-6.45" dx="6.2" dy="3.9" layer="1"/>
<smd name="1" x="8.95" y="-0.65" dx="3.3" dy="4.4" layer="1" rot="R90"/>
<wire x1="7.7" y1="-4.5" x2="-6.8" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-6.8" y1="-4.5" x2="-6.8" y2="4.5" width="0.127" layer="21"/>
<wire x1="-6.8" y1="4.5" x2="7.7" y2="4.5" width="0.127" layer="21"/>
<wire x1="7.7" y1="4.5" x2="7.7" y2="-4.5" width="0.127" layer="21"/>
<text x="-10" y="5" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SOP95P280X145-6N">
<smd name="2" x="-1.3" y="0" dx="1.1" dy="0.6" layer="1"/>
<smd name="5" x="1.3" y="0" dx="1.1" dy="0.6" layer="1"/>
<smd name="1" x="-1.3" y="0.95" dx="1.1" dy="0.6" layer="1"/>
<smd name="6" x="1.3" y="0.95" dx="1.1" dy="0.6" layer="1"/>
<smd name="3" x="-1.3" y="-0.95" dx="1.1" dy="0.6" layer="1"/>
<smd name="4" x="1.3" y="-0.95" dx="1.1" dy="0.6" layer="1"/>
<wire x1="-0.425" y1="1.45" x2="-0.425" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-0.425" y1="-1.45" x2="0.425" y2="-1.45" width="0.127" layer="21"/>
<wire x1="0.425" y1="-1.45" x2="0.425" y2="1.45" width="0.127" layer="21"/>
<wire x1="0.425" y1="1.45" x2="-0.425" y2="1.45" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.8" x2="2.1" y2="1.8" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.8" x2="2.1" y2="-1.8" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.8" x2="-2.1" y2="-1.8" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.8" x2="-2.1" y2="1.8" width="0.127" layer="51"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<circle x="-2.4" y="0.95" radius="0.25" width="0.127" layer="21"/>
</package>
<package name="TEENSY3.2">
<pad name="1" x="-16.63" y="-7.68" drill="0.8" diameter="1.65" shape="square"/>
<pad name="2" x="-14.07" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="3" x="-11.51" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="4" x="-8.95" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="5" x="-6.39" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="6" x="-3.83" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="7" x="-1.27" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="8" x="1.29" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="9" x="3.85" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="10" x="6.41" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="11" x="8.97" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="12" x="11.53" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="13" x="14.09" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="14" x="16.65" y="-7.68" drill="0.8" diameter="1.65"/>
<pad name="15" x="16.65" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="16" x="14.09" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="17" x="11.53" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="18" x="8.97" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="19" x="6.41" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="20" x="3.85" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="21" x="1.29" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="22" x="-1.27" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="23" x="-3.83" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="24" x="-6.39" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="25" x="-8.95" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="26" x="-11.51" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="27" x="-14.07" y="7.68" drill="0.8" diameter="1.65"/>
<pad name="28" x="-16.63" y="7.68" drill="0.8" diameter="1.65"/>
<wire x1="-19.21" y1="5.12" x2="-19.21" y2="-5.12" width="0.127" layer="21"/>
<wire x1="-19.21" y1="-5.12" x2="-12.8" y2="-5.12" width="0.127" layer="21"/>
<wire x1="-12.8" y1="-5.12" x2="-12.8" y2="5.12" width="0.127" layer="21"/>
<wire x1="-12.8" y1="5.12" x2="-19.21" y2="5.12" width="0.127" layer="21"/>
<wire x1="-18.415" y1="5.08" x2="-18.415" y2="8.89" width="0.127" layer="21"/>
<wire x1="-18.415" y1="8.89" x2="17.78" y2="8.89" width="0.127" layer="21"/>
<wire x1="17.78" y1="8.89" x2="17.78" y2="-8.89" width="0.127" layer="21"/>
<wire x1="17.78" y1="-8.89" x2="-18.415" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-18.415" y1="-8.89" x2="-18.415" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-18.415" y1="8.89" x2="-20.32" y2="8.89" width="0.127" layer="51"/>
<wire x1="-20.32" y1="8.89" x2="-20.32" y2="-8.89" width="0.127" layer="51"/>
<wire x1="-20.32" y1="-8.89" x2="17.78" y2="-8.89" width="0.127" layer="51"/>
<wire x1="17.78" y1="-8.89" x2="17.78" y2="8.89" width="0.127" layer="51"/>
<wire x1="17.78" y1="8.89" x2="-20.32" y2="8.89" width="0.127" layer="51"/>
<text x="-20.32" y="9.525" size="1.27" layer="25">&gt;NAME</text>
<text x="-20.32" y="-10.795" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CAPC1608X90N">
<description>0603</description>
<smd name="1" x="-0.75" y="0" dx="0.9" dy="0.95" layer="1"/>
<smd name="2" x="0.75" y="0" dx="0.9" dy="0.95" layer="1"/>
<wire x1="-1.45" y1="0.75" x2="1.45" y2="0.75" width="0.127" layer="21"/>
<wire x1="1.45" y1="0.75" x2="1.45" y2="-0.75" width="0.127" layer="21"/>
<wire x1="1.45" y1="-0.75" x2="-1.45" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-1.45" y1="-0.75" x2="-1.45" y2="0.75" width="0.127" layer="21"/>
<text x="-3.6" y="1.2" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL">
<pin name="1" x="0" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="2.54" size="1.9304" layer="125">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.9304" layer="127">&gt;VALUE</text>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.905" x2="4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="4.445" y1="-1.905" x2="4.445" y2="1.905" width="0.254" layer="94"/>
<wire x1="4.445" y1="1.905" x2="3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.254" layer="94"/>
</symbol>
<symbol name="HDRV2W80P254_1X2">
<wire x1="0" y1="7.62" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<pin name="1" x="7.62" y="5.08" length="short" rot="R180"/>
<pin name="2" x="7.62" y="2.54" length="short" rot="R180"/>
<text x="-2.54" y="10.16" size="1.9304" layer="125">&gt;NAME</text>
</symbol>
<symbol name="HDRV2W80P254_1X4">
<wire x1="0" y1="12.7" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="0" y2="12.7" width="0.254" layer="94"/>
<pin name="1" x="7.62" y="10.16" length="short" rot="R180"/>
<pin name="2" x="7.62" y="7.62" length="short" rot="R180"/>
<pin name="3" x="7.62" y="5.08" length="short" rot="R180"/>
<pin name="4" x="7.62" y="2.54" length="short" rot="R180"/>
<text x="-2.54" y="15.24" size="1.9304" layer="125">&gt;NAME</text>
</symbol>
<symbol name="SPST-NO">
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<pin name="1" x="0" y="0" visible="pin" length="short" direction="pas"/>
<pin name="2" x="15.24" y="0" visible="pin" length="short" direction="pas" rot="R180"/>
<wire x1="5.08" y1="0" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<text x="0" y="5.08" size="1.9304" layer="95">&gt;NAME</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="4.445" y1="1.27" x2="5.715" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.715" y1="-1.27" x2="6.985" y2="1.27" width="0.254" layer="94"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="-1.27" width="0.254" layer="94"/>
<wire x1="8.255" y1="-1.27" x2="9.525" y2="1.27" width="0.254" layer="94"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0" width="0.254" layer="94"/>
<pin name="1" x="12.7" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="0" y="0" visible="off" length="short" direction="pas"/>
<wire x1="3.81" y1="0" x2="3.175" y2="-1.27" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-0.635" y="1.905" size="1.27" layer="95">&gt;NAME</text>
<text x="-0.635" y="-3.175" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PSU_ADAPTER">
<wire x1="0" y1="15.24" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="43.18" y2="0" width="0.254" layer="94"/>
<wire x1="43.18" y1="0" x2="43.18" y2="15.24" width="0.254" layer="94"/>
<wire x1="43.18" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<text x="2.54" y="7.62" size="1.9304" layer="94">PSU - WALL MOUNT ADAPTER</text>
<text x="0" y="17.78" size="1.9304" layer="125">&gt;NAME</text>
</symbol>
<symbol name="ELECCAP">
<wire x1="2.2225" y1="1.27" x2="2.2225" y2="0" width="0.254" layer="94"/>
<wire x1="2.2225" y1="0" x2="2.2225" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.905" x2="0.635" y2="0.635" width="0.254" layer="94"/>
<pin name="1" x="0" y="0" visible="off" length="point" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<text x="-2.54" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<wire x1="3.175" y1="0" x2="3.175" y2="-0.635" width="0.254" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="3.81" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="3.175" y1="0" x2="3.175" y2="0.635" width="0.254" layer="94"/>
<wire x1="3.175" y1="0.635" x2="3.81" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="0" x2="2.2225" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="3.175" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="CAT24C512WI-GT3">
<pin name="A0" x="-2.54" y="10.16" length="short"/>
<pin name="A1" x="-2.54" y="7.62" length="short"/>
<pin name="A2" x="-2.54" y="5.08" length="short"/>
<pin name="VSS" x="-2.54" y="2.54" length="short" direction="pwr"/>
<pin name="VCC" x="17.78" y="10.16" length="short" direction="pwr" rot="R180"/>
<pin name="WP" x="17.78" y="7.62" length="short" rot="R180"/>
<pin name="SCL" x="17.78" y="5.08" length="short" rot="R180"/>
<pin name="SDA" x="17.78" y="2.54" length="short" rot="R180"/>
<wire x1="0" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="12.7" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="0" width="0.254" layer="94"/>
<text x="0" y="13.97" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PJ-036AH-SMT">
<wire x1="-2.54" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-8.255" y2="1.27" width="0.254" layer="94"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="1.27" width="0.254" layer="94"/>
<wire x1="-7.62" y1="1.27" x2="-6.985" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-12.065" y2="0" width="0.254" layer="94"/>
<wire x1="-12.065" y1="0" x2="-13.97" y2="1.905" width="0.254" layer="94"/>
<wire x1="-13.97" y1="1.905" x2="-15.24" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="-3.81" y1="6.35" x2="-5.08" y2="6.35" width="0.254" layer="94"/>
<wire x1="-5.08" y1="6.35" x2="-5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="6.35" x2="-14.605" y2="6.35" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-14.605" y2="3.81" width="0.254" layer="94"/>
<wire x1="-14.605" y1="3.81" x2="-15.875" y2="5.08" width="0.254" layer="94" curve="-90"/>
<wire x1="-15.875" y1="5.08" x2="-14.605" y2="6.35" width="0.254" layer="94" curve="-90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="3" x="0" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="2" x="0" y="0" visible="off" length="short" direction="pwr" rot="R180"/>
<text x="-18.415" y="7.62" size="2.54" layer="95">&gt;NAME</text>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="1.905" width="0.254" layer="94"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="SN74LVC1T45DBVR">
<pin name="VCCA" x="-2.54" y="7.62" length="short" direction="pwr"/>
<pin name="GND" x="-2.54" y="5.08" length="short" direction="pwr"/>
<pin name="A" x="-2.54" y="2.54" length="short"/>
<pin name="VCCB" x="22.86" y="7.62" length="short" direction="pwr" rot="R180"/>
<pin name="DIR" x="22.86" y="5.08" length="short" rot="R180"/>
<pin name="B" x="22.86" y="2.54" length="short" rot="R180"/>
<wire x1="0" y1="10.16" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="20.32" y2="10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="10.16" x2="0" y2="10.16" width="0.254" layer="94"/>
<text x="0" y="11.43" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TEENSY3.2">
<pin name="GND" x="-2.54" y="35.56" length="short" direction="pwr"/>
<pin name="DP0/RX1/TOUCH" x="-2.54" y="33.02" length="short"/>
<pin name="DP1/TX1/TOUCH" x="-2.54" y="30.48" length="short"/>
<pin name="DP2" x="-2.54" y="27.94" length="short"/>
<pin name="DP3/CANTX/PWM" x="-2.54" y="25.4" length="short"/>
<pin name="DP4/CANRX/PWM" x="-2.54" y="22.86" length="short"/>
<pin name="DP5/TX1/PWM" x="-2.54" y="20.32" length="short"/>
<pin name="DP6/PWM" x="-2.54" y="17.78" length="short"/>
<pin name="DP7/RX3/DOUT" x="-2.54" y="15.24" length="short"/>
<pin name="DP8/TX3/DIN" x="-2.54" y="12.7" length="short"/>
<pin name="DP9/RX2/CS/PWM" x="-2.54" y="10.16" length="short"/>
<pin name="DP10/TX2/CS/PWM" x="-2.54" y="7.62" length="short"/>
<pin name="DP11/DOUT" x="-2.54" y="5.08" length="short"/>
<pin name="DP12/DIN" x="-2.54" y="2.54" length="short"/>
<pin name="VIN" x="53.34" y="35.56" length="short" direction="pwr" rot="R180"/>
<pin name="AGND" x="53.34" y="33.02" length="short" direction="pwr" rot="R180"/>
<pin name="3.3VOUT" x="53.34" y="30.48" length="short" direction="pwr" rot="R180"/>
<pin name="DP23/A9/TOUCH/PWM" x="53.34" y="27.94" length="short" rot="R180"/>
<pin name="DP22/A8/TOUCH/PWM" x="53.34" y="25.4" length="short" rot="R180"/>
<pin name="DP21/A7/RX1/CS/PWM" x="53.34" y="22.86" length="short" rot="R180"/>
<pin name="DP16/A2/SCL0/TOUCH" x="53.34" y="10.16" length="short" rot="R180"/>
<pin name="DP20/A6/CS/PWM" x="53.34" y="20.32" length="short" rot="R180"/>
<pin name="DP19/A5/SCL0/TOUCH" x="53.34" y="17.78" length="short" rot="R180"/>
<pin name="DP18/A4/SDA0/TOUCH" x="53.34" y="15.24" length="short" rot="R180"/>
<pin name="DP17/A3/SDA0/TOUCH" x="53.34" y="12.7" length="short" rot="R180"/>
<pin name="DP15/A1/CS/TOUCH" x="53.34" y="7.62" length="short" rot="R180"/>
<pin name="DP14/A0/SCK" x="53.34" y="5.08" length="short" rot="R180"/>
<pin name="DP13/LED/SCLK" x="53.34" y="2.54" length="short" rot="R180"/>
<wire x1="0" y1="38.1" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="50.8" y2="0" width="0.254" layer="94"/>
<wire x1="50.8" y1="0" x2="50.8" y2="38.1" width="0.254" layer="94"/>
<wire x1="50.8" y1="38.1" x2="0" y2="38.1" width="0.254" layer="94"/>
<text x="0" y="39.37" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="CERCAP">
<wire x1="1.905" y1="1.27" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="0" x2="1.905" y2="-1.27" width="0.254" layer="94"/>
<pin name="1" x="0" y="0" visible="off" length="point" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<text x="-2.54" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.27" x2="3.175" y2="0" width="0.254" layer="94"/>
<wire x1="3.175" y1="0" x2="3.175" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="3.175" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CFS-20632768DZFB" prefix="Y">
<description>&lt;b&gt;CFS-20632768DZFB FOR TEENSY&lt;/b&gt; - CRYSTAL 32.7680KHZ 12.5PF T/H
 
&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;Frequency: 32.768kHz&lt;/li&gt;
&lt;li&gt;Frequency tolerance : ±20ppm&lt;/li&gt;	
&lt;li&gt;Load Capacitance: 12.5pF&lt;/li&gt;
&lt;li&gt;ESR (Equivalent Series Resistance): 35 kOhms &lt;/li&gt;
&lt;li&gt;Mounting type: Through hole&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/products/en?keywords=300-8303-nd"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR2PIN1ROW100MILHEADER" prefix="J">
<description>&lt;b&gt;2 pin 100 mil pitch headers&lt;/b&gt; 


&lt;p&gt;Exemple: &lt;a href="https://www.digikey.ca/product-detail/en/chip-quik-inc/HDR100IMP40F-G-V-TH/HDR100IMP40F-G-V-TH-ND/5978200"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="HDRV2W80P254_1X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV2W80P254_1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR4PIN1ROW100MILHEADER" prefix="J">
<description>&lt;b&gt;4 pin 100 mil pitch headers&lt;/b&gt; 


&lt;p&gt;Exemple: &lt;a href="https://www.digikey.ca/product-detail/en/chip-quik-inc/HDR100IMP40F-G-V-TH/HDR100IMP40F-G-V-TH-ND/5978200"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="HDRV2W80P254_1X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV2W80P254_1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PS1023ABLK" prefix="S">
<description>&lt;b&gt;PS1023ABLK&lt;/b&gt; - SWITCH PUSH SPST-NO 3A 125V
 
&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;Circuit : SPST-NO&lt;/li&gt;
&lt;li&gt;Switch Function: Off-Mom&lt;/li&gt;	
&lt;li&gt;Current Rating: 3A (AC)&lt;/li&gt;
&lt;li&gt;Voltage Rating - AC: 125V&lt;/li&gt;
&lt;li&gt;Panel Cutout Dimensions: Circular - 12.20mm Dia&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/product-detail/en/e-switch/PS1023ABLK/EG2001-ND/82862"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="SPST-NO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RC0805JR-074K7L" prefix="R" uservalue="yes">
<description>&lt;b&gt;RC0805JR-074K7L&lt;/b&gt; - SMD Resistor 4k7 ohm
 
&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;Resistance	: 4.7 kOhms&lt;/li&gt;
&lt;li&gt;Tolerance: ±5%&lt;/li&gt;	
&lt;li&gt;Power (Watts): 0.125W, 1/8W&lt;/li&gt;
&lt;li&gt;Package: 0805&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/product-detail/en/yageo/RC0805JR-074K7L/311-4.7KARCT-ND/731274"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="11.43" y="-1.27"/>
</gates>
<devices>
<device name="" package="CAPC2013X60N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="RESISTANCE" value="4k7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWI25-5-N-P5" prefix="U">
<description>&lt;b&gt;SWI25-5-N-P5&lt;/b&gt; - AC/DC WALL MOUNT ADAPTER 5V 20W
 
&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;Voltage input: 90 ~ 264 VAC&lt;/li&gt;
&lt;li&gt;Voltage output : 5V&lt;/li&gt;	
&lt;li&gt;Current output (max): 4A&lt;/li&gt;
&lt;li&gt;Power: 20W &lt;/li&gt;
&lt;li&gt;Connector: Barrel Plug, 2.1mm I.D. x 5.5mm O.D. x 9.5mm&lt;/li&gt;
&lt;li&gt;Polarization: Positive Center&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/product-detail/en/cui-inc/SWI25-5-N-P5/102-4196-ND/7070092"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="PSU_ADAPTER" x="-2.54" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10PK1000MEFC8X11.5" prefix="C" uservalue="yes">
<description>&lt;b&gt;10PK1000MEFC8X11.5&lt;/b&gt; - Electrolytic Cap 1000UF 10V Radial
 
&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;Capacity : 1000uF&lt;/li&gt;
&lt;li&gt;Tolerance: ±20%&lt;/li&gt;
&lt;li&gt;Voltage rated: 10V&lt;/li&gt;
&lt;li&gt;Package: Radial 8mm dia. x 13mm&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/product-detail/en/rubycon/10PK1000MEFC8X11.5/1189-1050-ND/3133980"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="ELECCAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPRD350W60D800H1300P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAT24C512WI-GT3" prefix="U">
<description>&lt;b&gt;CAT24C512WI-GT3&lt;/b&gt; - I2C EEPROM Memory IC 512Kb (64K x 8) I²C 1MHz 400ns 8-SOIC

&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;Memory Size: 512Kb (64K x 8)&lt;/li
&lt;li&gt;Clock Frequency: 1MHz&lt;/li&gt;
&lt;li&gt;Memory Interface: I²C&lt;/li&gt;
&lt;li&gt;Voltage - Supply: 1.8 V ~ 5.5 V&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/product-detail/en/on-semiconductor/CAT24C512WI-GT3/CAT24C512WI-GT3OSCT-ND/2699493"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CAT24C512WI-GT3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-8N">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PJ-036AH-SMT" prefix="J">
<description>&lt;b&gt;PJ-036AH-SMT&lt;/b&gt; - CONN PWR JACK 2X5.5MM
 
&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;Mating: 2.10mm ID (0.083"), 5.50mm OD (0.217")&lt;/li&gt;
&lt;li&gt;Rated Voltage: 12V&lt;/li&gt;	
&lt;li&gt;Rated Current: 5A&lt;/li&gt;
&lt;li&gt;Depth: 0.360" (9.15mm)&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/product-detail/en/cui-inc/PJ-036AH-SMT-TR/CP-036AHPJCT-ND/1530994"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="PJ-036AH-SMT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PJ-036AH-SMT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74LVC1T45DBVR" prefix="U">
<description>&lt;b&gt;SN74LVC1T45DBVR&lt;/b&gt; - IC TRNSLTR BIDIRECTIONAL SOT23-6	

 
&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;VCCA: 1.65V ~ 5.5V&lt;/li&gt;
&lt;li&gt;VCCB: 1.65V ~ 5.5V&lt;/li&gt;	
&lt;li&gt;Data Rate: 420Mbps&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.com/product-detail/en/texas-instruments/SN74LVC1T45DBVR/296-16843-1-ND/639459"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="SN74LVC1T45DBVR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP95P280X145-6N">
<connects>
<connect gate="G$1" pin="A" pad="3"/>
<connect gate="G$1" pin="B" pad="4"/>
<connect gate="G$1" pin="DIR" pad="5"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCCA" pad="1"/>
<connect gate="G$1" pin="VCCB" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TEENSY" prefix="U">
<description>&lt;h3&gt;TEENSY 3.2&lt;/h3&gt;
&lt;p&gt;K20 Teensy 3.2 Kinetis MCU 32-Bit ARM® Cortex®-M4 Embedded Evaluation Board
&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.pjrc.com/teensy/teensy31.html"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;Suppliers&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/product-detail/en/sparkfun-electronics/DEV-13736/1568-1231-ND/5721426"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.digikey.ca/product-detail/en/adafruit-industries-llc/2756/1528-2385-ND/6827117"&gt;Digikey w/ headers&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="TEENSY3.2" x="35.56" y="0"/>
</gates>
<devices>
<device name="" package="TEENSY3.2">
<connects>
<connect gate="G$1" pin="3.3VOUT" pad="26"/>
<connect gate="G$1" pin="AGND" pad="27"/>
<connect gate="G$1" pin="DP0/RX1/TOUCH" pad="2"/>
<connect gate="G$1" pin="DP1/TX1/TOUCH" pad="3"/>
<connect gate="G$1" pin="DP10/TX2/CS/PWM" pad="12"/>
<connect gate="G$1" pin="DP11/DOUT" pad="13"/>
<connect gate="G$1" pin="DP12/DIN" pad="14"/>
<connect gate="G$1" pin="DP13/LED/SCLK" pad="15"/>
<connect gate="G$1" pin="DP14/A0/SCK" pad="16"/>
<connect gate="G$1" pin="DP15/A1/CS/TOUCH" pad="17"/>
<connect gate="G$1" pin="DP16/A2/SCL0/TOUCH" pad="18"/>
<connect gate="G$1" pin="DP17/A3/SDA0/TOUCH" pad="19"/>
<connect gate="G$1" pin="DP18/A4/SDA0/TOUCH" pad="20"/>
<connect gate="G$1" pin="DP19/A5/SCL0/TOUCH" pad="21"/>
<connect gate="G$1" pin="DP2" pad="4"/>
<connect gate="G$1" pin="DP20/A6/CS/PWM" pad="22"/>
<connect gate="G$1" pin="DP21/A7/RX1/CS/PWM" pad="23"/>
<connect gate="G$1" pin="DP22/A8/TOUCH/PWM" pad="24"/>
<connect gate="G$1" pin="DP23/A9/TOUCH/PWM" pad="25"/>
<connect gate="G$1" pin="DP3/CANTX/PWM" pad="5"/>
<connect gate="G$1" pin="DP4/CANRX/PWM" pad="6"/>
<connect gate="G$1" pin="DP5/TX1/PWM" pad="7"/>
<connect gate="G$1" pin="DP6/PWM" pad="8"/>
<connect gate="G$1" pin="DP7/RX3/DOUT" pad="9"/>
<connect gate="G$1" pin="DP8/TX3/DIN" pad="10"/>
<connect gate="G$1" pin="DP9/RX2/CS/PWM" pad="11"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="28"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GRM188R71E104KA01D" prefix="C" uservalue="yes">
<description>&lt;b&gt;GRM188R71E104KA01D&lt;/b&gt; - Cer Cap 0.1UF 25V X7R 0603
 
&lt;p&gt;&lt;h4&gt;Technical Specifications:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;Capacity : 0.1uF&lt;/li&gt;
&lt;li&gt;Tolerance: ±10%&lt;/li&gt;
&lt;li&gt;Voltage rated: 25V&lt;/li&gt;
&lt;li&gt;Temp Coef: X7R&lt;/li&gt;
&lt;li&gt;Package: 0603&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
 
&lt;h4&gt;Suppliers:&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.digikey.com/products/en?keywords=490-1524-1-ND"&gt;Digikey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CERCAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC1608X90N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="Y1" library="ledcube" deviceset="CFS-20632768DZFB" device=""/>
<part name="J1" library="ledcube" deviceset="HDR2PIN1ROW100MILHEADER" device=""/>
<part name="J2" library="ledcube" deviceset="HDR2PIN1ROW100MILHEADER" device=""/>
<part name="J3" library="ledcube" deviceset="HDR4PIN1ROW100MILHEADER" device=""/>
<part name="J4" library="ledcube" deviceset="HDR4PIN1ROW100MILHEADER" device=""/>
<part name="S1" library="ledcube" deviceset="PS1023ABLK" device=""/>
<part name="R1" library="ledcube" deviceset="RC0805JR-074K7L" device="" value="4k7"/>
<part name="R2" library="ledcube" deviceset="RC0805JR-074K7L" device="" value="4k7"/>
<part name="U3" library="ledcube" deviceset="SWI25-5-N-P5" device=""/>
<part name="C1" library="ledcube" deviceset="10PK1000MEFC8X11.5" device="" value="1000uF"/>
<part name="U1" library="ledcube" deviceset="CAT24C512WI-GT3" device=""/>
<part name="J5" library="ledcube" deviceset="PJ-036AH-SMT" device=""/>
<part name="U2" library="ledcube" deviceset="SN74LVC1T45DBVR" device=""/>
<part name="U4" library="ledcube" deviceset="TEENSY" device=""/>
<part name="C2" library="ledcube" deviceset="GRM188R71E104KA01D" device=""/>
<part name="C3" library="ledcube" deviceset="GRM188R71E104KA01D" device=""/>
<part name="C4" library="ledcube" deviceset="GRM188R71E104KA01D" device=""/>
<part name="C5" library="ledcube" deviceset="GRM188R71E104KA01D" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="-60.96" y1="109.22" x2="-60.96" y2="53.34" width="0.1524" layer="97"/>
<wire x1="-60.96" y1="53.34" x2="-7.62" y2="53.34" width="0.1524" layer="97"/>
<wire x1="-7.62" y1="53.34" x2="-7.62" y2="109.22" width="0.1524" layer="97"/>
<wire x1="-7.62" y1="109.22" x2="-60.96" y2="109.22" width="0.1524" layer="97"/>
<text x="-60.96" y="106.68" size="1.778" layer="97">External</text>
</plain>
<instances>
<instance part="Y1" gate="G$1" x="-45.72" y="73.66"/>
<instance part="J1" gate="G$1" x="2.54" y="-5.08"/>
<instance part="J2" gate="G$1" x="2.54" y="27.94"/>
<instance part="J3" gate="G$1" x="2.54" y="43.18"/>
<instance part="J4" gate="G$1" x="2.54" y="60.96"/>
<instance part="S1" gate="G$1" x="-45.72" y="55.88"/>
<instance part="R1" gate="G$1" x="152.4" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="149.225" y="64.135" size="1.27" layer="95"/>
<attribute name="VALUE" x="149.225" y="53.975" size="1.27" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="144.78" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="140.335" y="64.135" size="1.27" layer="95"/>
<attribute name="VALUE" x="140.335" y="53.975" size="1.27" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="-58.42" y="83.82"/>
<instance part="C1" gate="G$1" x="20.32" y="60.96" smashed="yes" rot="R270">
<attribute name="VALUE" x="27.94" y="55.88" size="1.27" layer="96" rot="R180"/>
<attribute name="NAME" x="22.86" y="60.96" size="1.27" layer="95"/>
</instance>
<instance part="U1" gate="G$1" x="182.88" y="45.72" rot="R180"/>
<instance part="J5" gate="G$1" x="17.78" y="83.82"/>
<instance part="U2" gate="G$1" x="25.4" y="5.08"/>
<instance part="U4" gate="G$1" x="72.39" y="22.86"/>
<instance part="C2" gate="G$1" x="134.62" y="60.96" smashed="yes" rot="R270">
<attribute name="VALUE" x="129.54" y="63.5" size="1.27" layer="96" rot="R270"/>
<attribute name="NAME" x="137.16" y="58.42" size="1.27" layer="95"/>
</instance>
<instance part="C3" gate="G$1" x="154.94" y="33.02" smashed="yes" rot="R270">
<attribute name="VALUE" x="149.86" y="35.56" size="1.27" layer="96" rot="R270"/>
<attribute name="NAME" x="157.48" y="30.48" size="1.27" layer="95"/>
</instance>
<instance part="C4" gate="G$1" x="58.42" y="15.24" smashed="yes" rot="R270">
<attribute name="VALUE" x="53.34" y="17.78" size="1.27" layer="96" rot="R270"/>
<attribute name="NAME" x="60.96" y="12.7" size="1.27" layer="95"/>
</instance>
<instance part="C5" gate="G$1" x="15.24" y="15.24" smashed="yes" rot="R270">
<attribute name="VALUE" x="10.16" y="17.78" size="1.27" layer="96" rot="R270"/>
<attribute name="NAME" x="10.16" y="12.7" size="1.27" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="LED_DIN" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="11.43" y1="0" x2="10.16" y2="0" width="0.1524" layer="91"/>
<wire x1="11.43" y1="0" x2="11.43" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="A"/>
<wire x1="11.43" y1="7.62" x2="12.7" y2="7.62" width="0.1524" layer="91"/>
<label x="12.7" y="7.62" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="12.7" y1="7.62" x2="22.86" y2="7.62" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-2.54" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="7.62" width="0.1524" layer="91"/>
<junction x="12.7" y="7.62"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SDA"/>
<wire x1="165.1" y1="43.18" x2="144.78" y2="43.18" width="0.1524" layer="91"/>
<wire x1="144.78" y1="43.18" x2="134.62" y2="43.18" width="0.1524" layer="91"/>
<wire x1="134.62" y1="43.18" x2="134.62" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="DP17/A3/SDA0/TOUCH"/>
<wire x1="134.62" y1="38.1" x2="134.62" y2="35.56" width="0.1524" layer="91"/>
<wire x1="134.62" y1="35.56" x2="125.73" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="144.78" y1="53.34" x2="144.78" y2="43.18" width="0.1524" layer="91"/>
<junction x="144.78" y="43.18"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCL"/>
<wire x1="165.1" y1="40.64" x2="152.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="152.4" y1="40.64" x2="137.16" y2="40.64" width="0.1524" layer="91"/>
<wire x1="137.16" y1="40.64" x2="137.16" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="DP16/A2/SCL0/TOUCH"/>
<wire x1="137.16" y1="33.02" x2="125.73" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="40.64" width="0.1524" layer="91"/>
<junction x="152.4" y="40.64"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="3"/>
<wire x1="17.78" y1="86.36" x2="22.86" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="22.86" y1="86.36" x2="22.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="22.86" y1="83.82" x2="17.78" y2="83.82" width="0.1524" layer="91"/>
<label x="24.13" y="83.82" size="1.778" layer="95"/>
<wire x1="22.86" y1="83.82" x2="27.94" y2="83.82" width="0.1524" layer="91"/>
<junction x="22.86" y="83.82"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="10.16" y1="53.34" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
<wire x1="20.32" y1="53.34" x2="20.32" y2="55.88" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="10.16" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="20.32" y1="50.8" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
<junction x="20.32" y="53.34"/>
<pinref part="J3" gate="G$1" pin="3"/>
<wire x1="10.16" y1="48.26" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<wire x1="20.32" y1="48.26" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<junction x="20.32" y="50.8"/>
<pinref part="J3" gate="G$1" pin="4"/>
<wire x1="10.16" y1="45.72" x2="20.32" y2="45.72" width="0.1524" layer="91"/>
<wire x1="20.32" y1="45.72" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<junction x="20.32" y="48.26"/>
<wire x1="20.32" y1="45.72" x2="29.21" y2="45.72" width="0.1524" layer="91"/>
<junction x="20.32" y="45.72"/>
<label x="25.4" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="10.16" y1="30.48" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<label x="11.43" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="53.34" y1="10.16" x2="48.26" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="DIR"/>
<label x="49.53" y="10.16" size="1.778" layer="95"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="58.42" y1="10.16" x2="53.34" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="22.86" y1="10.16" x2="17.78" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<label x="17.78" y="10.16" size="1.778" layer="95"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="17.78" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="AGND"/>
<wire x1="125.73" y1="55.88" x2="132.08" y2="55.88" width="0.1524" layer="91"/>
<label x="127" y="55.88" size="1.778" layer="95"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="132.08" y1="55.88" x2="134.62" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="A2"/>
<wire x1="185.42" y1="40.64" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="187.96" y1="40.64" x2="187.96" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="A1"/>
<wire x1="187.96" y1="38.1" x2="185.42" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="A0"/>
<wire x1="187.96" y1="38.1" x2="187.96" y2="35.56" width="0.1524" layer="91"/>
<wire x1="187.96" y1="35.56" x2="185.42" y2="35.56" width="0.1524" layer="91"/>
<junction x="187.96" y="38.1"/>
<wire x1="187.96" y1="40.64" x2="187.96" y2="43.18" width="0.1524" layer="91"/>
<junction x="187.96" y="40.64"/>
<pinref part="U1" gate="G$1" pin="VSS"/>
<wire x1="187.96" y1="43.18" x2="185.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="35.56" x2="199.39" y2="35.56" width="0.1524" layer="91"/>
<junction x="187.96" y="35.56"/>
<label x="195.58" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="69.85" y1="58.42" x2="58.42" y2="58.42" width="0.1524" layer="91"/>
<label x="58.42" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="154.94" y1="27.94" x2="154.94" y2="25.4" width="0.1524" layer="91"/>
<wire x1="154.94" y1="25.4" x2="160.02" y2="25.4" width="0.1524" layer="91"/>
<label x="157.48" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="17.78" y1="88.9" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<label x="24.13" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="4"/>
<wire x1="10.16" y1="63.5" x2="20.32" y2="63.5" width="0.1524" layer="91"/>
<wire x1="20.32" y1="63.5" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="3"/>
<wire x1="20.32" y1="66.04" x2="10.16" y2="66.04" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="10.16" y1="68.58" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="20.32" y1="68.58" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
<junction x="20.32" y="66.04"/>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="10.16" y1="71.12" x2="20.32" y2="71.12" width="0.1524" layer="91"/>
<wire x1="20.32" y1="71.12" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
<junction x="20.32" y="68.58"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="20.32" y1="63.5" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<junction x="20.32" y="63.5"/>
<wire x1="20.32" y1="71.12" x2="29.21" y2="71.12" width="0.1524" layer="91"/>
<junction x="20.32" y="71.12"/>
<label x="26.67" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCCA"/>
<wire x1="22.86" y1="12.7" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<label x="17.78" y="12.7" size="1.778" layer="95"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="15.24" width="0.1524" layer="91"/>
<wire x1="17.78" y1="15.24" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="VIN"/>
<wire x1="125.73" y1="58.42" x2="132.08" y2="58.42" width="0.1524" layer="91"/>
<label x="127" y="58.42" size="1.778" layer="95"/>
<wire x1="132.08" y1="58.42" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="132.08" y1="60.96" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BTN" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="10.16" y1="33.02" x2="38.1" y2="33.02" width="0.1524" layer="91"/>
<wire x1="38.1" y1="33.02" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="DP6/PWM"/>
<wire x1="38.1" y1="40.64" x2="69.85" y2="40.64" width="0.1524" layer="91"/>
<label x="11.43" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCU_DOUT" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="B"/>
<wire x1="48.26" y1="7.62" x2="63.5" y2="7.62" width="0.1524" layer="91"/>
<wire x1="63.5" y1="7.62" x2="63.5" y2="27.94" width="0.1524" layer="91"/>
<wire x1="63.5" y1="27.94" x2="66.04" y2="27.94" width="0.1524" layer="91"/>
<label x="49.53" y="7.62" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="DP11/DOUT"/>
<wire x1="69.85" y1="27.94" x2="66.04" y2="27.94" width="0.1524" layer="91"/>
<wire x1="66.04" y1="27.94" x2="68.58" y2="27.94" width="0.1524" layer="91"/>
<wire x1="68.58" y1="27.94" x2="66.04" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<wire x1="53.34" y1="12.7" x2="48.26" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCCB"/>
<label x="49.53" y="12.7" size="1.778" layer="95"/>
<wire x1="53.34" y1="12.7" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="12.7" x2="55.88" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="55.88" y1="15.24" x2="58.42" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="3.3VOUT"/>
<wire x1="125.73" y1="53.34" x2="132.08" y2="53.34" width="0.1524" layer="91"/>
<label x="127" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="154.94" y1="33.02" x2="154.94" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="165.1" y1="35.56" x2="154.94" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="152.4" y1="66.04" x2="152.4" y2="69.85" width="0.1524" layer="91"/>
<wire x1="152.4" y1="69.85" x2="161.29" y2="69.85" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="144.78" y1="66.04" x2="144.78" y2="69.85" width="0.1524" layer="91"/>
<wire x1="144.78" y1="69.85" x2="152.4" y2="69.85" width="0.1524" layer="91"/>
<junction x="152.4" y="69.85"/>
<label x="157.48" y="69.85" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DP15/A1/CS/TOUCH"/>
<wire x1="125.73" y1="30.48" x2="139.7" y2="30.48" width="0.1524" layer="91"/>
<wire x1="139.7" y1="30.48" x2="139.7" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="WP"/>
<wire x1="139.7" y1="38.1" x2="165.1" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
